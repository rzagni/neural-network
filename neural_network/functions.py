"""
Module to store the commonly used activation functions, derivatives and related approximations.
"""
import numpy as np


def sigmoid(x: np.ndarray) -> np.ndarray:
    """Calculate the sigmoid function for any x. Sigmoid(x) = 1 / (1 + np.exp(-x))"""
    return 1 / (1 + np.exp(-x))


def sigmoid_no_overflow(x: np.ndarray) -> np.ndarray:
    """Calculate the sigmoid function when x is in -45..30 range, reducing extreme values to the boundary."""
    # limit values that could produce an overflow [sigmoid(-45) = 2.86251858e-20] ... [sigmoid(+30) = 1]
    x = np.where(x < -45, -45, np.where(x > 30, 30, x))
    return 1 / (1 + np.exp(-x))


def sigmoid_derivative_from_output(sigmoid_values) -> np.ndarray:
    """Calculate the value of the derivative (gradient) of the sigmoid for any sigmoid value."""
    return sigmoid_values * (1 - sigmoid_values)


def sigmoid_piecewise(x: np.ndarray, xmin=-2, xmax=2, x0=0.5) -> np.ndarray:
    """Approximate the sigmoid function with a straight line.

    Approximate with a straight line rising from 0 to 1 between xmin and xmax; flat 0 before xmin and +1 after xmax.

    :param x: the input we want to calculate the sigmoid for;
    :param xmin: the value to start the linear approximation; before this always return 0.
    :param xmax: the value to end the linear approximation; after this always return 1.
    :param x0: the value
    """
    assert xmin < xmax, 'xmin ({xmin}) should be strictly less than xmax ({xmax})'.format(xmin=xmin, xmax=xmax)
    return np.where(x < xmin, 0, np.where(x > xmax, 1, x / (xmax-xmin) + x0))


def sigmoid_piecewise_derivative(y: np.ndarray, safe_input=True) -> np.ndarray:
    """ Approximate the sigmoid function derivative from sigmoid own values with two straight lines.

    :param y: the sigmoid function values we want the derivative for;
    :param safe_input: if the function can assume input is in the range 0..1;
    """
    y_left = y * 0.5            # y / 2         the values if y is under 0.5 (then x was negative)
    y_right = 0.5 - y_left      # (1 - y) / 2   the values if y is above 0.5 (then x was positive)
    if safe_input:
        return np.where(y < 0.5, y_left, y_right)  # input should be bound in 0..1
    return np.where(y <= 0, 0, np.where(y >= 1, 0, np.where(y <= 0.5, y_left, y_right)))


def tanh(x: np.ndarray) -> np.ndarray:
    """Hyperbolic tanget activation function."""
    return np.tanh(x)


def tanh_deriv(x: np.ndarray) -> np.ndarray:
    """Derivative of the hyperbolic tanget activation function."""
    return 1.0 - np.tanh(x)**2


def step(x: np.ndarray) -> np.ndarray:
    """Calculate the step function: 0 when x <= 0 and +1 when x > 0."""
    return np.where(x <= 0, 0, 1)
