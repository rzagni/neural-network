"""
This module provides one or more kinds of neural network.
"""

import numpy as np
from neural_network.functions import sigmoid, sigmoid_derivative_from_output


class NN3Layer:
    """A neural network with 3 layers: inputs, hidden layer, outputs."""
    def __init__(self, input_size, output_size, neurons, bias=True):
        """Create a neural network with the desired dimensions.

        :param input_size: the number of inputs for every sample
        :param output_size: the number of outputs for every result
        :param neurons: the number of neurons in the hidden layer
        :param bias: to turn bias nodes on=1 (True) or off=0 (False)
        """
        self.input_size = input_size
        self.output_size = output_size
        self.num_neurons = neurons
        self.bias = bias
        self.backpropagate_delta = True

        # seed random numbers to make calculation deterministic
        np.random.seed(1)

        # randomly initialize weights between -1 and 1 with mean 0
        self.syn0 = 2 * np.random.random((input_size + 1, neurons)) - 1   # size +1 for bias
        self.syn1 = 2 * np.random.random((neurons + 1, output_size)) - 1  # size +1 for bias

    def train(self, examples: np.ndarray, results: np.ndarray, alpha=1, iterations=10000, print_error=True):
        """Train the network using the given samples and results."""
        assert examples.shape[1] == self.input_size
        assert results.shape[1] == self.output_size

        # Loop on activation + backpropagation to learn the coefficients
        for j in range(iterations):

            # Feed forward through layers 0, 1, and 2 to calculate the prediction with current weights
            l0 = self._add_bias(examples)                        # LAYER 0 = inputs + bias
            l1 = self._add_bias(sigmoid(np.dot(l0, self.syn0)))  # weighted linear combination of l0 & apply activation
            l2 = sigmoid(np.dot(l1, self.syn1))                  # weighted linear combination of l1 & apply activation

            # l2_error > 0 means expected result is higher than output; to raise it we'll increase the weights
            l2_error = results - l2

            # in what direction and how much should we move? l2_delta > 0 means the output should be raised.
            l2_delta = l2_error * sigmoid_derivative_from_output(l2)    # sigmoid derivative is between 0 and 0.25

            # Update neuron weights between L1 and L2; increase them if output should be increased, i.e if error is >0.
            self.syn1 += alpha * l1.T.dot(l2_delta)

            # distribute the L2 error on L1 according to the weights,
            # i.e proportional to how much each l1 value contribute to the l2 error
            if self.backpropagate_delta:
                l1_error = l2_delta.dot(self.syn1.T)  # backpropagate how much we want to move
            else:
                l1_error = l2_error.dot(self.syn1.T)  # backpropagate the unmodified error

            # in what direction is the target l1?
            l1_delta = l1_error * sigmoid_derivative_from_output(l1)

            # Update neuron weights, we increase them if output should be increased, i.e if error is >0.
            self.syn0 += alpha * l0.T.dot(l1_delta[:, :-1])     # remove bias of level 1

            # report on how we are going & eventually adapt alpha
            if print_error and (j % (iterations // 10)) == 0:
                error = np.mean(np.abs(l2_error))
                # alpha = self.adaptative_alpha(error, old_error, alpha) if 'old_error' in vars() else alpha    # noinspection PyUnboundLocalVariable
                # old_error = error
                print("Alpha: {a:f} \t{it:d} \tTraining Error: {e:7.5f}".format(a=alpha, it=j, e=error))

        if print_error:
            # Use learned parameters to calculate prediction and prediction error on training set
            prediction = self.predict(examples)
            prediction_error = abs_error(results, prediction)
            print("Alpha: {a:f} \t{it:d} \tTraining Error: {e:7.5f}".format(a=alpha, it=iterations, e=prediction_error))

    def adaptative_alpha(self, error, old_error, alpha):
        delta = (old_error - error) / old_error
        if -10 / 100 < delta < 1 / 100:
            alpha *= 99 / 100
        elif delta < -10 / 100:
            alpha *= 110 / 100
        return alpha

    def get_learned_weights(self) -> tuple:
        """Returns the matrices (no.ndarray) with the learnt weight for every layer, from input towards output."""
        return self.syn0, self.syn1

    def predict(self, inputs: np.ndarray) -> np.ndarray:
        """Use the network to predict the outputs for the given inputs."""
        l1 = sigmoid(np.dot(self._add_bias(inputs), self.syn0))     # inputs => hidden layer (l1)
        outputs = sigmoid(np.dot(self._add_bias(l1), self.syn1))    # hidden layer => output layer = prediction
        return outputs

    def _add_bias(self, _in):
        """Returns the given matrix with one extra column for the bias.
        If bias is on then the extra column is filled with ones, otherwise with zeros.
        """
        temp = np.ones([_in.shape[0], _in.shape[1] + 1]) if self.bias else np.zeros([_in.shape[0], _in.shape[1] + 1])
        temp[:, 0:-1] = _in  # putting the values back in their place, keeping the last column for bias
        return temp


def abs_error(expectations, predictions):
    return np.mean(np.abs(expectations - predictions))

