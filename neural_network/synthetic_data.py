"""
Classes to generate synthetic data from a pattern and some required variance.
"""

import numpy as np

MAX_STDEVS = 6


class Synthesizer:
    """Produces synthetic data by repeating the baseline pattern and superimposing a gaussian noise onto it.

    The produced data is mimicking a time serie where the baseline pattern repeats with some noise over it.
    """
    def __init__(self, baseline: list):
        """Create a synthesizer for the given baseline pattern."""
        self.baseline = baseline

    def generate(self, stdev: float, seed: int, repeats=1):
        """Produce synthetic data by repeating the baseline pattern and superimposing a gaussian noise onto it.

        :param stdev: the required standard deviation for the gaussian noise to superimpress to the pattern;
        :param seed: the seed for the random number generator to generate the noise;
        :param repeats: how many times the pattern has to be repeated;
        :returns: a one dimension numpy.ndarray with as many repetitions of the randomized pattern as required."""
        np.random.seed(seed)
        result = np.array(self.baseline * repeats)
        deltas = np.random.normal(0, stdev, size=result.size)
        return (result + deltas).clip(0, max(self.baseline) + MAX_STDEVS * stdev)


def bucketize(data, buckets_input, buckets_output, bucket_size):
    """Divide historical data in buckets of desired size and generate past(inputs) and future(predictions) training data set.

    :param data: the time-serie data to use to generate inputs and predictions;
    :param bucket_size: the number of samples of each bucket;
    :param buckets_input: the number of buckets to put in the input;
    :param buckets_output: the number of buckets to put in the prediction;
    :return: a matrix for inputs and one for predictions, each row being one example;
    """
    inputs, results = [], []
    weeks = data.size // bucket_size
    for first_week in range(weeks - (buckets_input + buckets_output) + 1):
        start_in = first_week * bucket_size
        end_in = start_in + buckets_input * bucket_size
        end_res = end_in + buckets_output * bucket_size
        inputs.append(data[start_in:end_in])
        results.append(data[end_in:end_res])
    return np.array(inputs), np.array(results)
