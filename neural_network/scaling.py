"""
Module to offer scaling utilities for input and output data.
"""


class Scale:
    """Scales linerly a given value from the origin interval to the destination interval."""
    def __init__(self, orig_interval_start, orig_interval_end, dest_interval_start, dest_interval_end):
        if orig_interval_start >= orig_interval_end:
            raise ValueError("Origin interval start must be less than interval end. Error start {} >= end {} !"
                             .format(orig_interval_start, orig_interval_end))
        if dest_interval_start >= dest_interval_end:
            raise ValueError("Destination interval start must be less than interval end. Error start {} >= end {} !"
                             .format(dest_interval_start, dest_interval_end))
        self.orig_interval_start = orig_interval_start
        self.orig_interval_end = orig_interval_end
        self.dest_interval_start = dest_interval_start
        self.dest_interval_end = dest_interval_end
        self.orig_interval = (self.orig_interval_end - self.orig_interval_start)
        self.dest_interval = (self.dest_interval_end - self.dest_interval_start)

    def scale(self, val):
        """To scale element e∈[a,b] to an interval [c,d] you have to calculate ((e−a)/(b−a))⋅(d−c)+c"""
        if not self.orig_interval_start <= val.all() <= self.orig_interval_end:
            raise ValueError("The give value {} is outside of the origin interval: [{}-{}]"
                             .format(val, self.orig_interval_start, self.orig_interval_end))
        return ((val - self.orig_interval_start) / self.orig_interval) * self.dest_interval + self.dest_interval_start

    def unscale(self, val):
        """Scale back to original interval from the destination interval."""
        return ((val - self.dest_interval_start) / self.dest_interval) * self.orig_interval + self.orig_interval_start
