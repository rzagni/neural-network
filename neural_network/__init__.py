"""The main package of the neural network, providing a vectorized implementation of a Neural Network."""

import os

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
with open(DIR_PATH + '/version.txt') as fp:
    VERSION = fp.read().strip()             # 0.0.1-1-xxxxxxxx
