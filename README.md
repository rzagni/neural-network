Neural Network
==============

This module provides a vectorized implementation of a Neural Network.

It has mostly basic functionalities, like Bias, and a few tools, like scaling and unscaling.

This NN is used to forecast the buildup of trash into a container given past levels.

Tools to generate simple synthetic data are provided to easily test the Neural Network.

Installation
--
Run `python setup.py install`


Running
--
The network module provides the class NN3Layer that implements a network with one configurable hidden layer 
and provides the straightforward methods `train` and `predict`.

The file `test_network.py` in the `test` directory uses py.test to run experiments on the NN
 using different combinations of parameters.

License
--
The contents of this project are available for personal, local, strictly non commercial use 
under the GNU General Public License (GPL) version 3. 
Licenses for public and/or commercial and/or networked use are available by contacting the author.
