import pathlib
import re
import subprocess

from setuptools import setup, find_packages

VERSION_PATH = pathlib.Path(__file__).parent / 'neural_network' / 'version.txt'
VERSION_REG_EXP = r'v?(\d+)\.(\d+)\.(\d+)'


def save_version_from_git_tag():
    try:
        subprocess.check_call(['git', 'rev-parse', '--git-dir'],
                              stdout=subprocess.DEVNULL,
                              stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        return

    with VERSION_PATH.open('w') as fp:
        cmd = ['git', 'describe', '--always', '--long']
        subprocess.check_call(cmd, stdout=fp)


def read_version():
    """Reads the version from the version file as single components"""
    with VERSION_PATH.open() as fp:
        version = fp.read().strip()             # 0.0.1-1-xxxxxxxx
        tag, *tail = version.split('-')[:2]     # 0.0.1, [1] // xxxxxxxx

        m = re.match(VERSION_REG_EXP, tag)   # 0.0.1 => (0) (0) (1)
        if not m:
            return 0, 0, 0, None

        major, minor, patch = [int(m.group(i)) for i in range(1, 4)]
        offset = tail and int(tail[0])
        return major, minor, patch, offset


def get_version(patch_level_increment=0):
    """Returns the current version string from info in the version file, eventually increasing the patch level.

    Version numbers 'major', 'minor' and 'patch_level' are straight values from the last git (annotated) tag,
    while 'offset' is optional ad represents the number of commits done after the last tag.

    :param patch_level_increment: the amount to increase the patch level in the returned string; default is zero.
    :return: the version string in the format <major>.<minor>.<patch>[.dev<offset_from_last_tag>]
    """
    try:
        major, minor, patch, offset = read_version()
        if offset:
            return '{}.{}.{}.dev{}'.format(major, minor, patch + patch_level_increment, offset)
        return '{}.{}.{}'.format(major, minor, patch + patch_level_increment)

    except FileNotFoundError as e:
        msg = "File {} is not found, and git data is unavailable"
        raise Exception(msg.format(e.filename))


def updated_version(patch_level_increment=0):
    save_version_from_git_tag()
    return get_version(patch_level_increment)

setup(
    name='neural-network',
    version=updated_version(),
    packages=find_packages(exclude=["tests", "tests.*"]),
    description='RZ neural network',
    keywords='neural-network NN machine-learning',
    author='Roberto Zagni',
    author_email='rz70rz70+py@gmail.com',
    url='https://bitbucket.org/rzagni/neural-network',
    setup_requires=[
        'setuptools >= 0.8',
        'wheel >= 0.26.0',
    ],
    install_requires=[
        'numpy >= 1.9.2',
        'pytest >= 2.9.1',
        # 'statsd >= 3.2.1',
    ],
    # package_data={'neural_network': ['*.txt']},
)
