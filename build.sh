#!/bin/bash

set -euf -o pipefail

echo =========== INSTALLING PREREQUISITES ==============

# get some prerequisites
pip install -v wheel setuptools numpy

echo =========== SETTING UP ==============

# NOTE: existing and active virtualenv is assumed!!
# install the package in develop mode (files are in-place)

pip install -v -e .
pip install -r test/test_requirements.txt

# python setup.py build_ext --inplace  # no ext build for now

#echo ===========LINTING FILES===========

# FIXME

#echo ===========RUNNING STATIC CODE CHECKS=======

# FIXME
