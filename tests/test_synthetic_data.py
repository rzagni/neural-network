import neural_network.data as sd


def test_generate():
    baseline = [100, 100, 100, 100, 100, 0, 0]
    synth = sd.Synthesizer(baseline)

    stdev = 9
    repeats = 52
    data = synth.generate(stdev, 1, repeats)

    assert data.size == len(baseline) * repeats
    assert min(data) >= 0
    assert max(data) <= max(baseline) + sd.MAX_STDEVS * stdev


def test_bucketize_data():
    baseline = [100, 100, 100, 100, 100, 0, 0]
    synth = sd.Synthesizer(baseline)

    stdev = 9
    repeats = 52
    data = synth.generate(stdev, 1, repeats)
    assert data.size == len(baseline) * repeats

    w_in, w_out, w_size = 4, 1, len(baseline)
    inputs, results = sd.bucketize(data, w_size, w_in, w_out)
    assert len(inputs) == len(results)
    assert len(inputs) + (w_in - 1) + w_out == repeats

    for i in range(repeats - (w_in + w_out) + 1):
        assert len(inputs[i]) == w_in * w_size
        assert len(results[i]) == w_out * w_size
