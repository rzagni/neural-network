# noinspection PyPackageRequirements
import pytest
import numpy as np

import neural_network.network as nn
import neural_network.data as nnd
from neural_network.scaling import Scale


@pytest.mark.parametrize("alphas, iterations, neurons", [
    ([1, 10], [1*1000, 2*1000, 5*1000, 10*1000], [2, 3, 4]),    # 2 neurons should be OK
])
def test_xor(alphas, iterations, neurons):
    print()
    for alpha in alphas:
        for num_iterations in iterations:
            for num_neurons in neurons:
                inputs = np.array([[0, 0],
                                   [0, 1],
                                   [1, 0],
                                   [1, 1],
                                   [1, 0],
                                   [1, 1],
                                   ])
                outputs = np.array([[0], [1], [1], [0], [1], [0], ])
                nn, predictions, num_training, error_all = \
                    train_and_test_nn(inputs, outputs, alpha, num_iterations, num_neurons)
                error_training = error_all[:num_training]
                avg_train_error = np.mean(np.abs(error_training))
                error_test = error_all[num_training:]
                avg_test_error = np.mean(np.abs(error_test))

                print("Alpha:\t{alpha:f}\tIterations:\t{it:d}\tNeurons:\t{n:d}\tSeed:\t{seed:2d}"
                      "\tTraining Error:\t{te:7.5f}\tPrediction Error:\t{pe:7.5f}"
                      .format(alpha=alpha, it=num_iterations, n=num_neurons, seed=0,
                              pe=avg_test_error, te=avg_train_error))


def get_weekly_data_24h_office(stdev=9, seed=1):
    synth = nnd.Synthesizer([100, 100, 100, 100, 100, 0, 0])
    data = synth.generate(stdev, seed, 52)
    w_in, w_out, w_size = 4, 1, 7
    return nnd.bucketize(data, w_size, w_in, w_out)


def get_weekly_data_4h_house(stdev=3, seed=1):
    synth = nnd.Synthesizer([
        0, 10, 20, 25, 40, 5,   # Mon
        0, 10, 20, 25, 40, 5,   # Tue
        0, 10, 20, 25, 40, 5,   # Wed
        0, 10, 20, 25, 40, 5,   # Thu
        0, 10, 20, 25, 40, 5,   # Fri
        0, 5, 25, 30, 30, 10,   # Sat
        0, 5, 25, 30, 30, 10,   # Sun
    ])   # amount of variance on every sample
    data = synth.generate(stdev, seed, 52)
    w_in, w_out, w_size = 4, 1, 7*6
    return nnd.bucketize(data, w_size, w_in, w_out)


def get_weekly_data_4h_office(stdev=3, seed=1):
    synth = nnd.Synthesizer([
        # 0 4   8  12  16  20   hour
        0, 10, 40, 40, 10, 0,   # Mon
        0, 10, 40, 40, 10, 0,   # Tue
        0, 10, 40, 40, 10, 0,   # Wed
        0, 10, 40, 40, 10, 0,   # Thu
        0, 10, 40, 40, 10, 0,   # Fri
        0,  0, 10, 10,  0, 0,   # Sat
        0,  0, 10, 10,  0, 0,   # Sun
    ])   # amount of variance on every sample
    data = synth.generate(stdev, seed, 52)
    w_in, w_out, w_size = 4, 1, 7*6
    return nnd.bucketize(data, w_size, w_in, w_out)


def get_weekly_data_1h_office(stdev=0.5, seed=1):
    synth = nnd.Synthesizer([
        # 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23
        0,   0, 0, 0, 0, 0, 0, 5, 5,10, 10, 10, 15, 15, 10, 10, 10,  5,  5,  5,  0,  0,  0,  0,   # Mon
        0,   0, 0, 0, 0, 0, 0, 5, 5,10, 10, 10, 15, 15, 10, 10, 10,  5,  5,  5,  0,  0,  0,  0,   # Tue
        0,   0, 0, 0, 0, 0, 0, 5, 5,10, 10, 10, 15, 15, 10, 10, 10,  5,  5,  5,  0,  0,  0,  0,   # Wed
        0,   0, 0, 0, 0, 0, 0, 5, 5,10, 10, 10, 15, 15, 10, 10, 10,  5,  5,  5,  0,  0,  0,  0,   # Thu
        0,   0, 0, 0, 0, 0, 0, 5, 5,10, 10, 10, 15, 15, 10, 10, 10,  5,  5,  5,  0,  0,  0,  0,   # Fri
        0,   0, 0, 0, 0, 0, 0, 0, 0, 5,  5,  5,  5, 10,  5,  5,  5,  0,  0,  0,  0,  0,  0,  0,   # Sat
        0,   0, 0, 0, 0, 0, 0, 0, 0, 5,  5,  5,  5, 10,  5,  5,  5,  0,  0,  0,  0,  0,  0,  0,   # Sun
    ])
    w_in, w_out, w_size, weeks = 4, 1, 7*24, 52
    data = synth.generate(stdev, seed, 52)
    return nnd.bucketize(data, w_size, w_in, w_out)


@pytest.mark.parametrize("data_generator, alphas, iterations, neurons, bias", [
    # (get_weekly_data_24h_office, np.arange(0.010, 0.090, 0.01), [1*1000, 2*1000, 5*1000, 10*1000], [3, 5, 7, 14, 21, 28]), # 7 * (4+1) = 35
    # (get_weekly_data_4h_office, np.arange(0.15, 0.25, 0.01), [50*1000], [6, 12, 24, 48, 96]),  # 6*7 *(4+1)
    # (get_weekly_data_4h_house, np.arange(0.15, 0.25, 0.01), [50*1000], [6, 12, 24, 48, 96]),
    (get_weekly_data_1h_office, [0.005], [5*1000], [24], True),  # 24*7 * (4+1) = 840
    ])
def test_nn3l_weekly(data_generator, alphas, iterations, neurons, bias):
    print()
    for alpha in alphas:
        for num_iterations in iterations:
            for num_neurons in neurons:
                for seed in range(5):
                    inputs, outputs = data_generator(seed=seed)
                    nn, predictions, num_training, error_all = \
                        train_and_test_nn(inputs, outputs, alpha, num_iterations, num_neurons, bias)
                    error_training = error_all[:num_training]
                    avg_train_error = np.mean(np.abs(error_training))
                    error_test = error_all[num_training:]
                    avg_test_error = np.mean(np.abs(error_test))

                    print("Alpha:\t{alpha:f}\tIterations:\t{it:5d}\tNeurons:\t{n:d}\tSeed:\t{seed:2d}"
                          "\tTraining Error:\t{te:7.5f}\tPrediction Error:\t{pe:7.5f}"
                          .format(alpha=alpha, it=num_iterations, n=num_neurons, seed=seed,
                                  pe=avg_test_error, te=avg_train_error))
                    # print_pred_vs_expect(outputs, predictions, num_training)


@pytest.mark.parametrize("data_generator, alphas, iterations, neurons", [
    # (get_weekly_data_24h_office, [0.4], 50*1000, []),
    # (get_weekly_data_4h_office, [0.1], 50*1000, []),
    # (get_weekly_data_4h_house, [0.1], 50*1000, []),
    (get_weekly_data_1h_office, [0.001], [5*1000], [24])
    ])
def test_nn3l_weekly_multiple_seeds(data_generator, alphas, iterations, neurons):
    print()
    for alpha in alphas:
        for num_iterations in iterations:
            for num_neurons in neurons:
                ms_inputs, ms_outputs = None, None
                for seed in range(10):
                    inputs, outputs = data_generator(seed=seed)
                    ms_inputs = np.append(ms_inputs, inputs, axis=0) if seed != 0 else inputs
                    ms_outputs = np.append(ms_outputs, outputs, axis=0) if seed != 0 else outputs
                    assert len(ms_outputs) == (1+seed) * len(outputs)

                nn, predictions, num_training, error_all = \
                    train_and_test_nn(ms_inputs, ms_outputs, alpha, num_iterations, num_neurons)
                error_training = error_all[:num_training]
                avg_train_error = np.mean(np.abs(error_training))
                error_test = error_all[num_training:]
                avg_test_error = np.mean(np.abs(error_test))

                print("Alpha:\t{alpha:f}\tIterations:\t{it:d}\tNeurons:\t{n:d}\tSeed:\t0..{seed:2d}"
                      "\tTraining Error:\t{te:7.5f}\tPrediction Error:\t{pe:7.5f}\tDELTA Error:\t{de:+7.5f}"
                      .format(alpha=alpha, it=num_iterations, n=num_neurons, seed=10,
                              pe=avg_test_error, te=avg_train_error, de=avg_test_error - avg_train_error))
                # print_pred_vs_expect(ms_outputs, predictions, num_training)


def train_and_test_nn(inputs, outputs, alpha=0.01, iterations=5000, num_neurons=20, bias=True):
    # Set up NN
    input_size = len(inputs[0]) if inputs.ndim > 1 else 1
    output_size = len(outputs[0]) if outputs.ndim > 1 else 1
    # num_neurons = input_size + output_size if input_size + output_size <= 50 else (input_size + output_size) // 2
    net = nn.NN3Layer(input_size, output_size, num_neurons, bias=bias)

    # Scale inputs and outputs
    min_in, max_in = min(inputs.min(), outputs.min()), max(inputs.max(), outputs.max())
    scale = Scale(min_in, max_in, 0, 1)  # original interval - network interval
    nn_inputs = scale.scale(inputs)
    nn_outputs = scale.scale(outputs)

    # Divide training set from test set and train
    num_training = (len(inputs) * 3) // 4
    net.train(nn_inputs[:num_training], nn_outputs[:num_training], alpha, iterations, print_error=False)

    # Calculate error on test set
    # nn_prediction = net.predict(nn_inputs[num_training:])
    # error = nn_outputs[num_training:] - nn_prediction
    nn_prediction = net.predict(nn_inputs)
    error_all = nn_outputs - nn_prediction

    # Scale back to original interval
    predictions = scale.unscale(nn_prediction)

    return net, predictions, num_training, error_all


def print_pred_vs_expect(expectations, predictions, num_training):
    """Display predictions VS 'real' outputs."""
    offset = len(expectations) - len(predictions)   # adjust indexing if expectations contains all outputs, not test only
    output_size = len(expectations[0])

    format_string = ""
    for i in range(output_size):
        format_string += ("{d[%d]:9.5f} " % i)

    for e in range(num_training, len(predictions)):
        print("Exp   %d: " % e, format_string.format(d=expectations[e + offset]))
        print("Pred  %d: " % e, format_string.format(d=predictions[e]))
        print("Delta %d: " % e, format_string.format(d=predictions[e] - expectations[e + offset]))
        print()
              # expectations[e + offset] * 100 // 1 / 100, "\n",
              # predictions[e] * 100 // 1 / 100)

