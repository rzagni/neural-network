import numpy as np
import scaling


def test_scale_1():
    scale = scaling.Scale(0, 100, 0, 1)
    orig = np.array([1, 10, 50, 100])
    expect = np.array([0.01, 0.1, 0.5, 1])

    verify_scale_unscale(scale, orig, expect)


def test_scale_2():
    scale = scaling.Scale(0, 100, 10, 20)
    orig = np.array([1, 10, 50, 100])
    expect = np.array([10.1, 11, 15, 20])

    verify_scale_unscale(scale, orig, expect)


def verify_scale_unscale(scale, original, expect):
    scaled = scale.scale(original)
    verify_almost_equal(expect, scaled)
    unscaled = scale.unscale(scaled)
    verify_almost_equal(original, unscaled)


def verify_almost_equal(expected, calculated):
    for o, u in zip(expected, calculated):
        assert abs(o - u) < 0.001
