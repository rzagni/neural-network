import re
import pytest

# from setup import VERSION_REG_EXP
VERSION_REG_EXP = r'v?(\d+)\.(\d+)\.(\d+)'


@pytest.mark.parametrize('version_string', ['8f088c0', '1.21-1', '1.21.-1'])
def test_re_incorrect_strings(version_string):
    m = re.match(VERSION_REG_EXP, version_string)
    assert m is None


@pytest.mark.parametrize('version_string',
                         ['0.0.1', '1.21.1', 'v0.1.29'])
def test_re_correct_strings(version_string):
    m = re.match(VERSION_REG_EXP, version_string)
    assert m is not None
    for i in range(1, 4):
        assert int(m.group(i)) >= 0
