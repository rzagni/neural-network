import neural_network.functions as nnf


def test_sigmoid():
    assert nnf.sigmoid(-60) <= 0 + 0.0001
    assert nnf.sigmoid(-10) <= 0 + 0.001
    assert nnf.sigmoid(-6) <= 0 + 0.003
    assert nnf.sigmoid(-3) <= 0 + 0.05
    assert nnf.sigmoid(-2) <= 0 + 0.2
    assert nnf.sigmoid(-1) <= 0 + 0.3
    assert nnf.sigmoid(0) == 0.5
    assert nnf.sigmoid(1) >= 1 - 0.3
    assert nnf.sigmoid(2) >= 1 - 0.2
    assert nnf.sigmoid(3) >= 1 - 0.05
    assert nnf.sigmoid(6) >= 1 - 0.003
    assert nnf.sigmoid(10) >= 1 - 0.001
    assert nnf.sigmoid(60) >= 1 - 0.0001


def test_sigmoid_derivative():
    assert nnf.sigmoid_derivative_from_output(0) == 0
    assert nnf.sigmoid_derivative_from_output(0.5) == 0.25
    assert nnf.sigmoid_derivative_from_output(1) == 0

    epsilon = 0.000001
    for val in range(-6, 7):
        s0 = nnf.sigmoid(val)
        s1 = nnf.sigmoid(val+epsilon)
        d = (s1-s0) / epsilon
        assert abs(d - nnf.sigmoid_derivative_from_output(s0)) < 0.001


